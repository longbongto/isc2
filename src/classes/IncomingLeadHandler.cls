/**
 * @description An implementation of IHandleMessages which validates the passed in Lead and inserts it.
 */
public with sharing class IncomingLeadHandler implements IHandleMessages {
    @testVisible private static final String INCOMING_LEAD_CHANNEL = 'IncomingLeads';
    @testVisible private static final String CHANNEL_PARAM;
    
    /**
     * @description Constructs an instance of IncomingLeadHandler.
     */
    public IncomingLeadHandler() {
    }

    /**
     * @description Handles a message on a subscribed channel.
     * @param channel The channel emitting the message.
     * @param data The accompanying data for the message.
     * @throws ArgumentNullException if channel is null.
     * @throws ArgumentException if the lead is missing a FirstName.
     */
    public void handleMessage(String channel, Object data) {
        ArgumentNullException.throwIfNull(channel, null);

        Lead newLead = (Lead)data;

        if (newLead.FirstName == null) throw new ArgumentException();

        try {
            insert newLead;       
        } catch (Exception e) {
            throw new ArgumentException();
        }
    }

    /**
     * @description Gets a list of channels an implementation subscribes to.
     * @return A List<String> of channel names this implementation is subscribed to.
     */
    public List<String> getSubscribedChannels() {
        List<String> result = new List<String>();

        result.add(INCOMING_LEAD_CHANNEL);

        return result;
    }
}