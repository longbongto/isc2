/**
 * @description A simple Pub/Sub pattern implementation
 */
public with sharing class PubSubService {
    private PubSubService() {
        SubscribedChannels = new List<String>(); 
    }

    public static List<String> SubscribedChannels {get; set;}

    /**
     * @description A singleton for service interaction.
     */
    public static PubSubService Instance {
        get {
            if (Instance == null) {
                Instance = new PubSubService();
            }

            return Instance;
        }

        private set;
    }

    /**
     * @description Subscribes a given IHandleMessages implementation to the channels it returns.
     * @param implementation An instance of IHandleMessages.
     * @throws ArgumentNullException if implementation is null.
     */
    public void subscribe(IHandleMessages implementation) {
        ArgumentNullException.throwIfNull(implementation, null);

        String handlerName = String.valueOf(implementation).substring(0,String.valueOf(implementation).indexOf(':'));

        IHandleMessages handler = (IHandleMessages)Type.forName(handlerName).newInstance();

        for (String s : handler.getSubscribedChannels()) {
            if (!SubscribedChannels.contains(s)) {
                SubscribedChannels.add(s);
            }
        }
    }

    /**
     * @description Un-subscribes a given IHandleMessages implementation to the channels it returns.
     * @param implementation An instance of IHandleMessages.
    * @throws ArgumentNullException if implementation is null.
     */
    public void unsubscribe(IHandleMessages implementation) {
        ArgumentNullException.throwIfNull(implementation, null);

        String handlerName = String.valueOf(implementation).substring(0,String.valueOf(implementation).indexOf(':'));

        IHandleMessages handler = (IHandleMessages)Type.forName(handlerName).newInstance();

        for (String s : handler.getSubscribedChannels()) {
            if (SubscribedChannels.contains(s)) {
                SubscribedChannels.remove(SubscribedChannels.indexOf(s));
            }
        }
    }

    /**
     * @description Emits a message to a given channel containing the specified data.
     * @param channel The channel to emit a message on.
     * @param data The data to emit.
     * @throws ArgumentNullException if channel is null.
     */
    public void emit(String channel, Object data) {
        ArgumentNullException.throwIfNull(channel, null);

        String handlerName;

        switch on channel {
            when 'IncomingLeads' {
                handlerName = 'IncomingLeadHandler';
            }
        }  

        if (handlerName == null) return;
        
        IHandleMessages handler = (IHandleMessages)Type.forName(handlerName).newInstance();

        if (SubscribedChannels.contains(channel)){
            handler.handleMessage(channel, data);
        }
    }
}